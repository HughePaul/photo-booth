var path = require('path');
var mkdirp = require('mkdirp');

// load config
var config = require('./package').photobooth;

// make config paths absolute
config.appDirectory = path.resolve(__dirname, config.appDirectory);
mkdirp(path.join(config.appDirectory, config.photoDirectory));

config.tempDirectory = path.resolve(__dirname, config.tempDirectory);
mkdirp(config.tempDirectory);

// create camera
var Camera = require('./camera');
var camera = new Camera(config);

// create API
var API = require('./api');
var api = new API(config, camera);

// allow the app to be loaded over http too:
api.app.use('/', require('express').static(config.appDirectory));

console.log('Server Started');