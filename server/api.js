var express = require('express');
var socketio = require('socket.io');
var childProcess = require('child_process');

var API = function(config, camera) {
    this.config = config;
    this.camera = camera;

    // create listening server
    this.app = express();
    var server = this.app.listen(this.config.port, this.config.host);

    // start socket.io server
    this.io = socketio.listen(server);
    this.io.sockets.on('connection', this.newSocketClient.bind(this));

    // validate id param
    this.app.param('id', function(req, res, next, id) {
        next(
            /^\d+$/.exec(String(id)) ?
            undefined :
            new Error('Invalid ID'));
    });

    // add route for downloading a photo
    this.app.get('/photo/:id', this.getPhotoFile.bind(this));

    // add route for downloading a thumbnail
    this.app.get('/thumb/:id', this.getThumbnailFile.bind(this));

    this.camera.setOnUpdatePhoto(this.updatePhoto.bind(this));
};

API.prototype.newSocketClient = function(socket) {
    socket.on('listPhotos', this.listPhotos.bind(this));
    socket.on('takePhoto', this.takePhoto.bind(this));
    socket.on('deletePhoto', this.deletePhoto.bind(this));
    socket.on('shutdown', this.shutdown.bind(this));
    socket.on('exit', function() {
        process.exit();
    });
};

API.prototype.shutdown = function(data, callback) {
    if (typeof callback !== 'function') {
        return;
    }
    childProcess.exec("osascript -e 'tell app \"System Events\" to shut down'", function(err) {
        if (err) {
            console.error('Error shutting down:', err);
        }
        err = err && err.message || err || null;
        callback(err, photos);
    });
};

API.prototype.listPhotos = function(data, callback) {
    console.log('Request: listPhotos', data);
    this.camera.getPhotoList(function(err, photos) {
        if (err) {
            console.error('Error listing photo:', err);
        }
        err = err && err.message || err || null;
        callback(err, photos);
    });
};

API.prototype.takePhoto = function(data, callback) {
    var that = this;

    console.log('Request: takePhoto', data);

    this.camera.takePhoto(function(err, id, thumb, photo) {
        if (err) {
            console.error('Error taking photo:', err);
        }
        err = err && err.message || err || null;
        console.log('Photo taken:', 'Err:', err, 'id:', id, 'filename:', thumb, photo);
        that.io.sockets.emit('addPhoto', err, id, thumb, photo);
        return callback && callback(err, id, thumb, photo);
    });
};

API.prototype.updatePhoto = function(err, id, thumb, photo) {
    err = err && err.message || err || null;
    this.io.sockets.emit('updatePhoto', err, id, thumb, photo);
};

API.prototype.deletePhoto = function(data, callback) {
    var that = this;
    console.log('Request: deletePhoto', data);
    this.camera.deletePhoto(data.photoId, function(err, id) {
        err = err && err.message || err || null;
        that.io.sockets.emit('deletePhoto', err, id);
        return callback && callback(err, id);
    });
};

API.prototype.getPhotoFile = function(req, res) {
    var filename = this.camera.getPhotoFilename(req.params.id, true);
    res.sendfile(filename);
};

API.prototype.getThumbnailFile = function(req, res) {
    var filename = this.camera.getThumbnailFilename(req.params.id, true);
    res.sendfile(filename);
};

module.exports = API;