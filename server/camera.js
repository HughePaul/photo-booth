var fs = require('fs');
var path = require('path');
var gm = require('gm');

var Camera = function(config) {
	this.config = config;
};

function pad(num, width) {
	width = width || 2;
	num = '' + num;
	while (num.length < width) {
		num = '0' + num;
	}
	return num;
}

function datestamp() {
	var now = new Date();
	return pad(now.getFullYear(), 4) +
		pad(now.getMonth() + 1) +
		pad(now.getDate()) +
		pad(now.getHours()) +
		pad(now.getMinutes()) +
		pad(now.getSeconds());
}

Camera.prototype.getPhotoFilename = function(photoId, full) {
	var filename = path.join(this.config.photoDirectory, 'photo' + photoId + '.jpg');
	if (full) {
		return path.resolve(this.config.appDirectory, filename);
	}
	return filename;
};

Camera.prototype.getThumbnailFilename = function(photoId, full) {
	var filename = path.join(this.config.photoDirectory, 'thumb' + photoId + '.jpg');
	if (full) {
		return path.resolve(this.config.appDirectory, filename);
	}
	return filename;
};

var rePhoto = new RegExp('^thumb([0-9]+)\\.jpg$');
Camera.prototype.getPhotoList = function(callback) {
	var that = this;
	var photoPath = path.join(this.config.appDirectory, this.config.photoDirectory);
	fs.readdir(photoPath, function(err, files) {
		if (err) {
			return callback(err);
		}
		var photos = [];
		for (var i = 0; i < files.length; i++) {
			var m = rePhoto.exec(files[i]);
			if (m) {
				var id = m[1];
				photos.push({
					photoId: id,
					url: that.getPhotoFilename(id, false),
					thumb: that.getThumbnailFilename(id, false)
				});
			}
		}
		callback(null, photos);
	});
};

Camera.prototype.deletePhoto = function(id, callback) {
	// var filename = this.getPhotoFilename(id, true);
	// if (!filename) {
	// 	return callback(new Error('Cannot find photo'));
	// }
	// fs.unlink(filename, function(err) {});
	var thumbnail = this.getThumbnailFilename(id, true);
	if (!thumbnail) {
		return callback(new Error('Cannot find thumbnail'));
	}
	fs.unlink(thumbnail, function(err) {});
	callback(null, id);
};

Camera.prototype.getDevice = function(callback) {
	if (this.device) {
		return callback(null, this.device);
	}

	var gphoto2;
	var GPhoto;
	try {
		gphoto2 = require('gphoto2');
		GPhoto = new gphoto2.GPhoto2();
	} catch (e) {
		return callback(e);
	}

	// List cameras / assign list item to variable to use below options
	GPhoto.list(function(list) {
		if (list.length === 0) {
			var err = new Error('No cameras found!');
			console.error('List devices error:', err);
			return callback(err);
		}

		var device = list[0];
		console.log('Camera found:', device.model);

		device.getConfig(function(err, settings) {
			if (err) {
				console.error('Camera settings error:', err);
				return callback(err);
			}
			console.log('Camera settings:', settings);

			this.device = device;

			callback(null, device);

		});

	});
};

Camera.prototype.makeThumbnail = function(src, dest, callback) {
	try {
		gm(src)
			.resize(480)
			.noProfile()
			.write(dest, callback);
	} catch (e) {
		callback(e);
	}
};

Camera.prototype.setOnUpdatePhoto = function(handler) {
	this.onUpdatePhoto = handler;
};

Camera.prototype.takePhoto = function(callback) {
	var that = this;
	console.log('takePhoto');

	var photoId = datestamp();

	var temptemplate = this.config.tempDirectory + '/photo.XXXXXX';
	var filename = this.getPhotoFilename(photoId, true);
	var thumbnail = this.getThumbnailFilename(photoId, true);
	var photoName = that.getPhotoFilename(photoId);

	this.getDevice(function(err, device) {
		if (err) {
			console.error('Get device error:', err);
			return callback(err);
		}

		device.takePicture({
			targetPath: temptemplate
		}, function(err, tmpname) {
			if (err) {
				console.error('Take picture error:', err);
				return callback(err);
			}
			console.log('Image captured');

			if (that.onUpdatePhoto) {
				callback(null, photoId, null, photoName);
				callback = that.onUpdatePhoto;
			}

			console.log('Resizing image for thumbnail', thumbnail);
			that.makeThumbnail(tmpname, thumbnail, function(err) {
				if (err) {
					console.error('Error making thumbnail:', err);
					return callback(err);
				}

				console.log('Renaming image to', filename);
				fs.rename(tmpname, filename, function() {
					if (err) {
						console.error('Rename file error:', err);
						return callback(err);
					}

					console.log('done');
					var thumbName = that.getThumbnailFilename(photoId);
					callback(null, photoId, thumbName, photoName);

				});
			});
		});
	});
};

module.exports = Camera;