cd $(dirname "$0")

export PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/X11/bin

mkdir -p logs

while true
do

	echo Killing Chrome
	killall -TERM node 'Google Chrome' 2>/dev/null
	sleep 2
	killall -9 node 'Google Chrome' 2>/dev/null

	# Start the browser (See http://peter.sh/experiments/chromium-command-line-switches/)
	(
		sleep 1 && \
		echo Starting Chrome && \
		( /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome \
		 --no-default-browser-check --load-and-launch-app=app/ >> logs/app.log  2>&1 ) \
	) &

	echo Starting server
	/usr/local/bin/node ./server >> logs/server.log 2>&1
	echo Server terminated

done

