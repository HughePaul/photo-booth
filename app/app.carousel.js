window.App = window.App || {};

App.Carousel = function(container, maxPhotos) {
	var that = this;

	this.maxPhotos = maxPhotos || null;

	this.carousel = $('<div>')
		.appendTo(container)
		.slick({
			infinite: false,
			arrows: false,
			dots: false,
			draggable: false,
			swipeMove: false,
			slidesToShow: 7,
			slidesToScroll: 1,
			centerMode: true,
			focusOnSelect: true
		});

};

App.Carousel.prototype.count = function() {
	return $('div.slick-slide', this.carousel).length;
};

App.Carousel.prototype.remove = function(id) {
	var that = this;
	console.log('Removing photo from carousel:', id);

	var slide = $('#photo' + id);
	if (!slide.length) {
		console.error('Slide not found:', id);
	}

	var photo = $('.photo', slide);
	if (!photo.length) {
		console.error('Photo not found:', id);
	}

	var index = +slide.attr('index');

	photo.fadeOut(500, function() {
		that.carousel
			.slickRemove(index);

		var count = that.count();

		var next = index;
		if (next >= count) {
			next = count - 1;
		}
		if (next >= 0) {
			window.setTimeout(function() {
				that.carousel
					.slickGoTo(next);
			});
		}

		console.log('Carousel photo removed', id);

		if (that.onDeleteCallback) {
			that.onDeleteCallback(id);
		}
	});
};

App.Carousel.prototype.goToEnd = function() {
	console.log('Carousel go to end');
	this.carousel
		.slickGoTo(this.count() - 1);
};

App.Carousel.prototype.set = function(id, url) {
	var photo = $('#photo' + id);
	if (!photo.length) {
		console.error('Photo not found:', id);
	}
	console.log('Updating carousel photo:', id, url);
	var img = $('img', photo);
	img
		.attr('src', url + '?' + Date.now())
		.addClass('mirror');
};

App.Carousel.prototype.add = function(id, url) {
	var that = this;

	var mirror = true;
	if (!url) {
		url = 'assets/loading.png';
		mirror = false;
	}

	var slide = $('<div>')
		.attr('id', 'photo' + id);

	var photo = $('<div>')
		.addClass('photo')
		.appendTo(slide);

	var img = $('<img>')
		.attr('src', url + '?' + Date.now())
		.appendTo(photo);
	if (mirror) {
		img.addClass('mirror');
	}

	var deleteButton = $('<div>')
		.addClass("photoDeleteButton")
		.appendTo(photo);
	deleteButton.get(0).addEventListener('click', function(event) {
		console.log('Clicked on delete', id);
		that.remove(id);
	});

	// remove first image if we are at photo limit
	if (this.maxPhotos && this.count() >= this.maxPhotos) {
		this.carousel.slickRemove(0);
	}

	this.carousel
		.slickAdd(slide.get(0));

	console.log('Carousel photo added', id, url);
};

App.Carousel.prototype.setOnDelete = function(callback) {
	this.onDeleteCallback = callback;
};