window.App = window.App || {};

App.Toast = function(text) {
	var that = this;

	console.error(text);

	this.container = $('<div>')
		.text(text)
		.addClass('toast')
		.appendTo('body');

	window.setTimeout(function() {
		that.fade();
	}, 3000);

};

App.Toast.prototype.fade = function() {
	var that = this;

	that.container
		.fadeOut(500, function() {
			window.setTimeout(function() {
				that.remove();
			}, 1000);
		});

};

App.Toast.prototype.remove = function() {
	this.container.remove();
};
