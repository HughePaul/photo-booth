window.App = window.App || {};

$(function() {

	// default server endpoint
	var serverEndpoint = 'http://127.0.0.1:8585';
	var maxPhotos = 20;


	var socket;

	function initSocket() {
		console.log('Init Socket');
		socket = io.connect(serverEndpoint);

		socket.on('updatePhoto', updatePhoto);
	}

	function initWebcam() {
		console.log('Init Webcam');
		navigator.webkitGetUserMedia({
			audio: false,
			video: true
		}, function(stream) {
			$('#webcamView').attr('src', webkitURL.createObjectURL(stream));
		}, function(err) {
			if (err) {
				new App.Toast('Error creating webcam view: ' + err);
				return console.error('Error creating webcam view:', err);
			}
		});
	}

	var carousel;

	function initCarousel() {
		carousel = new App.Carousel($('#photoCarousel'), maxPhotos);
		carousel.setOnDelete(deletePhoto);
	}

	function listPhotos() {
		console.log('List Photos');
		socket.emit('listPhotos', {}, function(err, photos) {
			if (err) {
				return console.error(err);
			}
			photos = photos.slice(photos.length - maxPhotos);
			photos.forEach(function(photo) {
				carousel.add(photo.photoId, photo.thumb);
			});
			carousel.goToEnd();
		});
	}

	var photoLock = false;

	function takePhoto(cb) {
		if (photoLock) {
			return new App.Toast('Already taking photo!');
		}
		console.log('Take Photo');
		photoLock = true;
		socket.emit('takePhoto', {}, function(err, photoId, url) {
			photoLock = false;
			if (err) {
				new App.Toast('Error taking photo: ' + err);
				return cb && cb(err);
			}
			console.log('Photo Taken! ID:', photoId, url);
			carousel.add(photoId, url);
			carousel.goToEnd();
			return cb && cb(null, photoId, url);
		});
	}

	function updatePhoto(err, photoId, url) {
		if (err) {
			return new App.Toast('Error processing photo on server: ' + err);
		}
		if (carousel) {
			console.log('Update Photo');
			carousel.set(photoId, url);
		}
	}

	function deletePhoto(id) {
		socket.emit('deletePhoto', {
			photoId: id
		}, function(err, id) {
			if (err) {
				return new App.Toast('Error deleting photo: ' + err);
			}
			console.log('Photo deleted', id);
		});
	}

	function shutdown() {
		console.log('Shutdown');
		socket.emit('shutdown', {}, function(err) {
			if (err) {
				console.error('Error shutting down:', err);
			}
		});
	}

	function exit(callback) {
		console.log('Exit');
		socket.emit('exit', {}, function(err) {
			if (err) {
				console.error('Error exiting:', err);
			}
		});
	}

	$('#takePhotoButton').click(function() {
		$('#photoCarousel').fadeOut();
		new App.CountDown(3, function() {
			takePhoto(function() {
				$('#photoCarousel').fadeIn();
			});
		});
	});

	$('#closeButton').click(exit);

	$('#shutdownButton').click(shutdown);

	// if the background is clicked then shrink the centered image
	$('#webcamView').click(function() {
		$('.slick-center').removeClass('slick-center');
	});

	initSocket();
	initWebcam();
	initCarousel();
	listPhotos();


});