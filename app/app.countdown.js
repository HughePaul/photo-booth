window.App = window.App || {};

App.CountDown = function(count, cb) {
	this.count = count || 3;
	this.callback = cb;

	this.doFlash = true;

	this.container = $('<div>')
		.addClass('countdownContainer')
		.appendTo('body');

	this.next();
};

App.CountDown.prototype.next = function() {
	var that = this;

	if (this.count === 0) {

		if (this.doFlash) {
			this.flash();
		}

		if (this.callback) {
			this.callback();
		}
		this.callback = null;

		this.remove();

		return;
	}

	console.log('Next', this.count);
	var number = $('<div>')
		.addClass("countdownNumber")
		.text(this.count)
		.appendTo(this.container)
		.fadeOut(1500, function() {
			window.setTimeout(function() {
				number.remove();
			}, 3000);
		});

	this.count--;

	if (this.count >= 0) {
		this.schedule();
	}
};

App.CountDown.prototype.flash = function() {
	console.log('Flash');
	this.container
		.addClass('countdownFlash');
};

App.CountDown.prototype.remove = function() {
	var that = this;
	window.setTimeout(function() {
		that.container.remove();
	}, 2000);
};

App.CountDown.prototype.schedule = function() {
	var that = this;
	window.setTimeout(function() {
		that.next();
	}, 1000);
};