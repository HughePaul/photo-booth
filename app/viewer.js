window.App = window.App || {};

$(function() {

	var socket;
	var latestPhoto;
	var latestUrl;

	var delay = 5000;
	var transition = 2000;

	var photos = {};

	function initSocket() {
		console.log('Init Socket');
		socket = io.connect();

		socket.on('connect', listPhotos);
		socket.on('addPhoto', addPhoto);
		//socket.on('updatePhoto', addPhoto);
		socket.on('deletePhoto', deletePhoto);
	}

	function listPhotos() {
		console.log('List Photos');
		socket.emit('listPhotos', {}, function(err, newPhotos) {
			if (err) {
				return console.error(err);
			}
			photos = {};
			newPhotos.forEach(function(photo) {
				photos[photo.photoId] = photo.url;
			});

			slideShowNext();
		});
	}

	function addPhoto(err, photoId, thumb, url) {
		if (err) {
			return console.error('add', err);
		}

		photos[photoId] = url;

		slideShowNext(photoId);
	}

	function deletePhoto(err, id) {
		if (err) {
			return console.error('delete', err);
		}

		delete photos[id];
	}

	function slideShowStart() {
		window.setInterval(function() {
			slideShowNext();
		}, delay);
	};

	function slideShowNext(id) {
		if (!id) {
			var ids = Object.keys(photos);
			console.log('Photos', ids.length);
			var index = Math.floor(Math.random() * ids.length);
			id = ids[index];
		}

		var url = photos[id];

		console.log('Photo', id, url);

		if (url && url !== latestUrl) {
			showPhoto(url);
			latestUrl = url;
		}

	}

	function showPhoto(url) {
		if (!url) return;
		console.log('Showing', url);
		var div = $('<div>')
			.addClass('photo')
			.css('background-image', 'url(' + url + ')')
			.appendTo('body')
			.hide();
		window.setTimeout(function() {
			div.ready(function() {
				console.log('fading in', url);
				div.fadeIn(transition, function() {
					console.log('faded in', url);
					if(latestPhoto)
						latestPhoto.remove();
					latestPhoto = div;
				});
			});
		}, 200);
	}


	slideShowStart();

	initSocket();


});