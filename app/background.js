chrome.app.runtime.onLaunched.addListener(function() {
	var windowid = chrome.app.window.create('booth.html', {
		id: "MainWindow",
		frame: "none",
		bounds: {
			width: 1024,
			height: 768
		}
	});
	chrome.windows.update(windowid, {
		state: "fullscreen"
	});
});