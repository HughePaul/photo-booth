cd $(dirname "$0")

set -e 
set -x

echo Updating from git
git pull

cd server

test '!' -f .package.json.cache || diff -q package.json .package.json.cache || (
  echo Install node modules
  cp -af package.json .package.json.cache
  npm install
)


echo Testing node install
npm test

cd ..

cp -f com.hughepaul.photobooth.plist ~/Library/LaunchAgents/

echo UPDATED
